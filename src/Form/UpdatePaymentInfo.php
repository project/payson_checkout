<?php

declare(strict_types=1);

namespace Drupal\payson_checkout\Form;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a Payson Checkout form.
 */
final class UpdatePaymentInfo extends FormBase implements ContainerInjectionInterface {

  public function __construct(public EntityTypeManagerInterface $entityTypeManager) {}

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'payson_checkout_update_payment_info';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, OrderInterface $commerce_order = NULL): array {
    if (empty($commerce_order)) {
      $this->messenger()->addError($this->t('Order not found.'));
      $form_state->setRedirect('<front>');
    }
    $form_state->set('commerce_order', $commerce_order);

    $payments = $this->getPayments($commerce_order);
    foreach ($payments as $payment) {
      $form['payments'][$payment->id()] = [
        '#type' => 'item',
        '#markup' => $this->t('Payment with Remote ID: @remoteId', ['@remoteId' => $payment->getRemoteId()]),
      ];
    }

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Update information'),
      ],
    ];

    return $form;
  }

  private function getPayments(OrderInterface $commerce_order) {
    $payments = $this->entityTypeManager->getStorage('commerce_payment')
      ->loadByProperties(['order_id' => $commerce_order->id()]);
    foreach ($payments as $id => $payment) {
      if ($payment->getPaymentGateway()->getPluginId() != 'payson_checkout') {
        unset($payments[$id]);
      }
    }

    return $payments;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\commerce_order\Entity\Order $commerce_order */
    $commerce_order = $form_state->get('commerce_order');
    $payments = $this->getPayments($commerce_order);

    foreach ($payments as $payment) {
      // Fake request to send in order number and remote id
      $request = new Request();
      $request->query->set('order_id', $commerce_order->id());
      $request->query->set('checkout', $payment->getRemoteId());
      /** @var \Drupal\payson_checkout\Plugin\Commerce\PaymentGateway\PaysonCheckoutPaymentGateway $plugin */
      $plugin = $payment->getPaymentGateway()->getPlugin();
      $plugin->onNotify($request);
      $this->messenger()
        ->addStatus($this->t('Payment with Remote ID: @remoteId updated.', ['@remoteId' => $payment->getRemoteId()]));
    }
  }

}
