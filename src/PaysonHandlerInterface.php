<?php

namespace Drupal\payson_checkout;

use Drupal\commerce_payment\Entity\Payment;

/**
 * Provides a handler for Payson requests.
 */
interface PaysonHandlerInterface {

  /**
   * Constructor.
   */
  public function __construct();

  /**
   * Get checkout array from Payson.
   *
   * @param \Drupal\commerce_payment\Entity\Payment $payment
   *   Payment to fetch Payson checkout order for.
   *
   * @return array
   *   Payson array of data.
   */
  public function getCheckout(Payment $payment): array;

  /**
   * Creates a payment for the order and checkout in Payson.
   *
   * @param \Drupal\commerce_payment\Entity\Payment $payment
   *   Payment to create Payson checkout for.
   * @param string $checkoutUri
   *   Checkout Uri used when going back to Drupal when canceling the payment.
   * @param string $returnUri
   *   Return Uri used the Drupal confirmation page.
   *
   * @return array
   *   Payson array of data.
   */
  public function createCheckout(Payment $payment, string $checkoutUri, string $returnUri): array;

  /**
   * Updates status for Payson checkout.
   *
   * @param \Drupal\commerce_payment\Entity\Payment $payment
   *   Payment in Drupal.
   * @param string $status
   *   New status.
   *
   * @return array
   *   Payson array of data.
   */
  public function updateStatus(Payment $payment, string $status): array;

}
