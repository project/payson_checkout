<?php

namespace Drupal\payson_checkout\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\payson_checkout\PaysonHandler;

/**
 * Payson Checkout with embedded payson form.
 */
class PaysonCheckoutPaymentForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    if (!$payment->getOrder()->getTotalPrice()->isPositive()) {
      throw new \Exception('Order ID "' . $payment->getOrderId() . '": Total price is not a positive amount.');
    }

    $paysonHandler = new PaysonHandler();
    $paysonCheckout = $paysonHandler->getCheckout($payment);
    if (empty($paysonCheckout)) {
      $paysonCheckout = $paysonHandler->createCheckout($payment, $form['#cancel_url'], $form['#return_url']);
      if (!empty($paysonCheckout['id'])) {
        $payment->setRemoteId($paysonCheckout['id']);
        $payment->save();
      }
    }

    if (empty($paysonCheckout['snippet']) || empty($paysonCheckout['id'])) {
      \Drupal::messenger()->addError('Could not create checkout at Payson.');
      return $form;
    }

    if (!empty($paysonCheckout['remote_state'])) {
      $payment->setRemoteState($paysonCheckout['status']);
      $payment->setExpiresTime($paysonCheckout['expirationTime']);
      $payment->save();
    }

    // Div in form that will be replaced with payson checkout form via JS.
    $form['payson'] = [
      '#prefix' => "<div class='payson-form'>",
      '#suffix' => '</div>',
      '#markup' => $paysonCheckout['snippet'],
    ];

    // And include the Js.
    $configuration = $payment->getPaymentGateway()
      ->getPlugin()
      ->getConfiguration();
    if ($configuration['mode'] == 'live') {
      $form['#attached']['library'][] = 'payson_checkout/checkout_live';
    }
    else {
      $form['#attached']['library'][] = 'payson_checkout/checkout_dev';
    }

    // No need to call buildRedirectForm as we are embedding an iframe.
    return $form;
  }

}
