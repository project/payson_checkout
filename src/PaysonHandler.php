<?php

namespace Drupal\payson_checkout;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\Entity\Payment;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

/**
 * Payson handler for reading and create checkouts at Payson.
 */
class PaysonHandler implements PaysonHandlerInterface {

  /**
   * Guzzle client.
   *
   * @var \GuzzleHttp\Client
   */
  protected Client $httpClient;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->httpClient = \Drupal::httpClient();
  }

  /**
   * Creates an authorisation string.
   *
   * @return string
   *   The base64 encoded string used for authorisation.
   */
  protected function authorisationString(Payment $payment) {
    $configuration = $payment->getPaymentGateway()
      ->getPlugin()
      ->getConfiguration();
    if ($configuration['mode'] == 'live') {
      return base64_encode($configuration['agent_id'] . ':' . $configuration['api_key']);
    }
    return base64_encode($configuration['agent_id_sand'] . ':' . $configuration['api_key_sand']);
  }

  /**
   * Get correct url for API calls depending on mode.
   *
   * @return string
   *   API endpoint url.
   */
  protected function apiUrl(Payment $payment): string {
    if ($payment->getPaymentGateway()
        ->getPlugin()
        ->getConfiguration()['mode'] == 'live') {
      return 'https://api.payson.se/2.0/';
    }
    return 'https://test-api.payson.se/2.0/';
  }

  /**
   * {@inheritdoc}
   */
  public function getCheckout(Payment $payment): array {
    if (empty($payment->getRemoteId())) {
      return [];
    }
    try {
      $checkout = $this->httpClient->request('GET', $this->apiUrl($payment) . 'Checkouts/' . $payment->getRemoteId(), [
        'headers' => [
          'Authorization' => 'Basic ' . $this->authorisationString($payment),
          'Content-Type' => 'application/json',
        ],
      ]);
    }
    catch (\Exception $e) {
      if ($e->getResponse()->getStatusCode()) {
        return [];
      }
    }

    if ($checkout->getStatusCode() == '200') {
      $body = $checkout->getBody()->getContents();
      $paysonCheckoutObj = json_decode($body, TRUE);
      if ($paysonCheckoutObj) {
        return $paysonCheckoutObj;
      }
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createCheckout(Payment $payment, string $checkoutUri, string $returnUri): array {
    $order = $payment->getOrder();
    /** @var \Drupal\payson_checkout\Plugin\Commerce\PaymentGateway\PaysonCheckoutPaymentGateway $gateway */
    $gateway = $payment->getPaymentGateway()->getPlugin();
    $configuration = $gateway->getConfiguration();
    $postData = [];

    $postData['merchant'] = [
      'checkoutUri' => $checkoutUri,
      'confirmationUri' => $returnUri,
      'notificationUri' =>
        $gateway->getNotifyUrl()->toString() .
        '?order_id=' . $payment->getOrderId(),
      'termsUri' => $configuration['terms'],
      'reference' => t('Order id @orderid at @storename', [
        '@orderid' => $payment->getOrderId(),
        '@storename' => $order->getStore()->getName(),
      ])->render(),
    ];

    $postData['order'] = [
      'currency' => $order->getTotalPrice()->getCurrencyCode(),
      'items' => $this->orderItems($order),
    ];

    $supported_locales = ['sv', 'en', 'fi', 'no', 'da', 'es', 'de'];
    $current_locale = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $postData['gui'] = [
      'colorScheme' => $configuration['theme'],
      'requestPhone' => $configuration['phone_required'] > 0 ? 1 : 0,
      'phoneOptional' => $configuration['phone_required'] == 2 ? 1 : 0,
      'verification' => $configuration['verification'],
      'locale' => in_array($current_locale, $supported_locales) ? $current_locale : 'en',
    ];

    $billingAddress = $order->getBillingProfile()->get('address')->first();
    $postData['customer'] = [
      'city' => $billingAddress->getLocality(),
      'countryCode' => $billingAddress->getCountryCode(),
      'email' => $order->getEmail(),
      'firstName' => $billingAddress->getGivenName(),
      'lastName' => $billingAddress->getFamilyName(),
      'phone' => '',
      'postalCode' => $billingAddress->getPostalCode(),
      'street' => $billingAddress->getAddressLine1(),
    ];

    try {
      $checkout = $this->httpClient->request('POST', $this->apiUrl($payment) . 'Checkouts', [
        'headers' => [
          'Authorization' => 'Basic ' . $this->authorisationString($payment),
          'Content-Type' => 'application/json',
        ],
        'body' => json_encode($postData),
      ]);

      if ($checkout->getStatusCode() == '201') {
        // Success.
        $body = $checkout->getBody()->getContents();
        $paysonCheckoutObj = json_decode($body, TRUE);
        if ($paysonCheckoutObj) {
          return $paysonCheckoutObj;
        }
      }
    }
    catch (ClientException $ex) {
      \Drupal::logger('payson_checkout')
        ->error('Could not create checkout at Payson. Error: %message', [
          '%message' => $ex->getMessage(),
        ]);
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function updateStatus(Payment $payment, string $status): array {
    $data = $this->getCheckout($payment);
    if (!empty($data['status']) && !empty($payment->getRemoteId())) {
      $data['status'] = $status;
      try {
        $checkout = $this->httpClient->request('PUT', $this->apiUrl($payment) . 'Checkouts/' . $payment->getRemoteId(), [
          'headers' => [
            'Authorization' => 'Basic ' . $this->authorisationString($payment),
            'Content-Type' => 'application/json',
          ],
          'body' => json_encode($data),
        ]);

        if ($checkout->getStatusCode() == '200') {
          // Success.
          $body = $checkout->getBody()->getContents();
          $paysonCheckoutObj = json_decode($body, TRUE);
          if ($paysonCheckoutObj) {
            return $paysonCheckoutObj;
          }
        }
      }
      catch (ClientException $ex) {
        \Drupal::logger('payson_checkout')
          ->error('Could not update checkout at Payson. Error: %message', [
            '%message' => $ex->getMessage(),
          ]);
      }
    }
    return [];
  }

  /**
   * Create items row for Payson data.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   Order to create rows from.
   *
   * @return array
   *   Array of items.
   */
  protected function orderItems(Order $order) {
    $checkoutItems = [];
    // Add all order items to the checkout.
    foreach ($order->getItems() as $row => $order_item) {
      // Default unit price.
      $checkoutItems[$row]['unitPrice'] = $order_item->getUnitPrice()
        ->getNumber();
      foreach ($order_item->getAdjustments() as $adjustment) {
        if ($adjustment->getType() == 'tax' && $adjustment->isIncluded()) {
          // Tax and it is included in the price.
          $checkoutItems[$row]['unitPrice'] = $order_item->getUnitPrice()
            ->getNumber();
          $checkoutItems[$row]['taxRate'] = $adjustment->getPercentage();
        }
        elseif ($adjustment->getType() == 'tax' && !$adjustment->isIncluded()) {
          // Tax that is not included in the price.
          $checkoutItems[$row]['unitPrice'] =
            $order_item->getUnitPrice()->getNumber() +
            $adjustment->getAmount()->getNumber();
          $checkoutItems[$row]['taxRate'] = $adjustment->getPercentage();
        }
      }
      $checkoutItems[$row]['name'] = $order_item->getTitle();
      $checkoutItems[$row]['quantity'] = $order_item->getQuantity();
      $checkoutItems[$row]['reference'] = $order_item->getPurchasedEntity()
        ->getSku();
      $checkoutItems[$row]['type'] = 'physical';
    }
    $adjustmentRow = count($checkoutItems) - 1;

    // Now add all the adjustments on the order items.
    // Discounts and fees and so on.
    foreach ($order->getItems() as $row => $order_item) {
      // Fix all adjustments that isn't tax.
      foreach ($order_item->getAdjustments() as $adjustment) {
        if ($adjustment->getType() == 'tax') {
          // We took tax in the previous loop.
          continue;
        }
        if ($adjustment->isIncluded()) {
          // We don't need to do anything if it is included in the price.
          continue;
        }
        $adjustmentRow++;
        switch ($adjustment->getType()) {
          case 'promotion':
            $checkoutItems[$adjustmentRow]['type'] = 'discount';
            break;

          case 'fee':
          case 'custom':
          default:
            if ($adjustment->getAmount()->getNumber() < 0) {
              // If negative set as discount otherwise set it as fee.
              $checkoutItems[$adjustmentRow]['type'] = 'discount';
            }
            else {
              $checkoutItems[$adjustmentRow]['type'] = 'fee';
            }
            break;
        }
        $checkoutItems[$adjustmentRow]['unitPrice'] = $adjustment->getAmount()
          ->getNumber();
        $checkoutItems[$adjustmentRow]['name'] = $adjustment->getLabel();
        $checkoutItems[$adjustmentRow]['quantity'] = $order_item->getQuantity();
      }
    }

    $adjustmentRow = count($checkoutItems) - 1;
    // Order adjustments.
    foreach ($order->getAdjustments() as $row => $orderAdjustment) {
      $adjustmentRow++;
      if (!$orderAdjustment->isIncluded()) {
        // Included is already included in total so we dont need to care about.
        switch ($orderAdjustment->getType()) {
          case 'fee':
          case 'custom':
          case 'promotion':
          case 'tax':
          default:
            if ($orderAdjustment->getAmount()->getNumber() < 0) {
              // If negative set as discount otherwise set it as fee.
              $checkoutItems[$adjustmentRow]['type'] = 'discount';
            }
            else {
              $checkoutItems[$adjustmentRow]['type'] = 'fee';
            }
            break;
        }
        $checkoutItems[$adjustmentRow]['unitPrice'] = $orderAdjustment->getAmount()
          ->getNumber();
        $checkoutItems[$adjustmentRow]['name'] = $orderAdjustment->getLabel();
        $checkoutItems[$adjustmentRow]['quantity'] = 1;
      }
    }
    return $checkoutItems;
  }

}
