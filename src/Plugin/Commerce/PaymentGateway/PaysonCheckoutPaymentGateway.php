<?php

namespace Drupal\payson_checkout\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;
use Drupal\payson_checkout\PaysonHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Provides the Payson checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "payson_checkout",
 *   label = @Translation("Payson Checkout"),
 *   modes = {
 *     "test" = @Translation("Sandbox"),
 *     "live" = @Translation("Live"),
 *   },
 *   display_label = @Translation("Payson"),
 *   forms = {
 *     "offsite-payment" =
 *   "Drupal\payson_checkout\PluginForm\OffsiteRedirect\PaysonCheckoutPaymentForm",
 *   },
 *   credit_card_types = {
 *     "mastercard", "visa", "amex",
 *   },
 * )
 */
class PaysonCheckoutPaymentGateway extends OffsitePaymentGatewayBase {

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.channel.payson_checkout');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
        'agent_id_sand' => '4',
        'api_key_sand' => '2acab30d-fe50-426f-90d7-8c60a7eb31d4',
        'currency_code' => 'sek',
        'theme' => 'blue',
        'order_state' => 'authorization',
        'verification' => 'none',
        'phone_required' => 0,
        'log' => 0,
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['agent_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Agent-ID'),
      '#description' => $this->t('Your Payson API User ID.'),
      '#default_value' => $this->configuration['agent_id'],
      '#required' => TRUE,
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API-key'),
      '#description' => $this->t('Your Payson API-key.'),
      '#default_value' => $this->configuration['api_key'],
      '#required' => TRUE,
    ];

    $form['agent_id_sand'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Agent-ID sandbox'),
      '#description' => $this->t('Payson API user id for sandbox. <br> Create your own sandbox account for testing at <a href="https://test-www.payson.se/testaccount/create/" target="_blank">Paysons test page</a>.'),
      '#default_value' => $this->configuration['agent_id_sand'],
      '#required' => TRUE,
    ];

    $form['api_key_sand'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sandbox API-key'),
      '#description' => $this->t('Payson API-key for sandbox.'),
      '#default_value' => $this->configuration['api_key_sand'],
      '#required' => TRUE,
    ];

    $form['terms'] = [
      '#type' => 'url',
      '#title' => $this->t('Link to terms and conditions'),
      '#description' => $this->t('Path to terms and conditions page including scheme and domain name.'),
      '#default_value' => $this->configuration['terms'],
      '#required' => TRUE,
    ];

    $form['invoice_terms'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Invoice terms'),
      '#description' => $this->t('<a href="https://www.payson.se/sv/foretag-betallosningar/payson-fakturavillkor/">Show the text with terms for Payson Invoice</a> that must be on checkout. This can be hidden if you show them in some other way.'),
      '#default_value' => $this->configuration['invoice_terms'],
      '#required' => TRUE,
    ];

    $form['theme'] = [
      '#type' => 'select',
      '#title' => $this->t('Theme'),
      '#description' => $this->t('Select the theme you want on the checkout. When changing theme keep in mind that the theme is stored in the checkout at Payson so all checkouts that are already created will not be affected.'),
      '#default_value' => $this->configuration['theme'],
      '#options' => [
        'blue' => $this->t('Blue form on white background (default).'),
        'gray' => $this->t('White form on gray background.'),
        'white' => $this->t('White form on white background.'),
        'GrayTextLogos' => $this->t('White form on gray background with text bank logotypes.'),
        'BlueTextLogos' => $this->t('Blue form on white background with text bank logotypes.'),
        'WhiteTextLogos' => $this->t('White form on white background with text bank logotypes.'),
        'GrayNoFooter' => $this->t('Gray form on white background with no bank logotypes and no footer.'),
        'BlueNoFooter' => $this->t('Blue form on white background with no bank logotypes and no footer.'),
        'WhiteNoFooter' => $this->t('White form on white background with no bank logotypes and no footer.'),
      ],
      '#required' => TRUE,
    ];

    $form['order_state'] = [
      '#type' => 'select',
      '#title' => $this->t('Order state on payment'),
      '#description' => $this->t('Order state to set when order gets status "Ready to ship". If set to authorization, payment will get status completed when order has been marked as shipped.'),
      '#default_value' => $this->configuration['order_state'],
      '#options' => [
        'authorization' => $this->t('Authorization'),
        'completed' => $this->t('Completed'),
      ],
      '#required' => TRUE,
    ];

    $form['verification'] = [
      '#type' => 'select',
      '#title' => $this->t('Use bank-id verification.'),
      '#default_value' => $this->configuration['verification'],
      '#options' => [
        'none' => $this->t('No'),
        'bankid' => $this->t('Use Bank id'),
      ],
      '#required' => TRUE,
    ];

    $form['phone_required'] = [
      '#type' => 'select',
      '#title' => $this->t('Request phone number'),
      '#default_value' => $this->configuration['phone_required'],
      '#options' => [
        '0' => $this->t('No'),
        '1' => $this->t('Yes'),
        '2' => $this->t('Optional'),
      ],
      '#required' => TRUE,
    ];

    $form['log'] = [
      '#type' => 'select',
      '#title' => $this->t('Extended logging'),
      '#default_value' => $this->configuration['log'],
      '#options' => [
        '0' => $this->t('No'),
        '1' => $this->t('Yes'),
      ],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['agent_id'] = $values['agent_id'];
    $this->configuration['api_key'] = $values['api_key'];
    $this->configuration['agent_id_sand'] = $values['agent_id_sand'];
    $this->configuration['api_key_sand'] = $values['api_key_sand'];
    $this->configuration['terms'] = $values['terms'];
    $this->configuration['invoice_terms'] = $values['invoice_terms'];
    $this->configuration['theme'] = $values['theme'];
    $this->configuration['order_state'] = $values['order_state'];
    $this->configuration['verification'] = $values['verification'];
    $this->configuration['phone_required'] = $values['phone_required'];
    $this->configuration['log'] = $values['log'];
  }

  public function onReturn(OrderInterface $order, Request $request) {
    if ($this->configuration['log']) {
      $this->logger->debug('onReturn for order: %order_id (%state)', [
        '%order_id' => $order->id(),
        '%state' => $order->getState()->getId(),
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    $this->messenger()
      ->addMessage($this->t('You have canceled checkout at @gateway but may resume the checkout process here when you are ready.', [
        '@gateway' => $this->getDisplayLabel(),
      ]));
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    $order_id = $request->query->get('order_id');

    if (empty($order_id)) {
      $this->logger->notice('IPN URL accessed with no data.');
      throw new BadRequestHttpException('IPN URL accessed with no data.');
    }
    $order = Order::load($order_id);
    if (empty($order)) {
      $this->logger->warning('Could not load order with order_id: %order_id', ['%order_id' => $order_id]);
      throw new BadRequestHttpException('IPN URL accessed with no valid order.');
    }
    $remoteId = $request->query->get('checkout');
    if (empty($remoteId)) {
      $this->logger->warning('No remoteId found in request.');
      throw new BadRequestHttpException('No remoteId found in request.');
    }
    if ($this->configuration['log']) {
      $this->logger->debug('onNotify for order: %order_id (%state) RemoteId: %remote_id', [
        '%order_id' => $order_id,
        '%state' => $order->getState()->getId(),
        '%remote_id' => $remoteId,
      ]);
    }

    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    /** @var \Drupal\commerce_payment\Entity\Payment $payment */
    $payment = $payment_storage->loadByRemoteId($remoteId);
    if (empty($payment)) {
      $this->logger->warning('No payment found with remoteId: %remote_id', ['%remote_id' => $remoteId]);
      throw new BadRequestHttpException('No payment found with remoteId.');
    }

    // Get checkout from payson and update the state.
    $paysonHandler = new PaysonHandler();
    $paysonCheckout = $paysonHandler->getCheckout($payment);

    if (empty($paysonCheckout)) {
      // Checkout don't exist.
      $payment->delete();
      $this->logger->warning('Payson Checkout does not exist. Payment: %payment', ['%payment' => $payment->id()]);
      return NULL;
    }
    if ($this->configuration['log']) {
      $this->logger->debug('PaysonCheckout: %status - %remoteId', [
        '%remoteId' => $remoteId,
        '%status' => $paysonCheckout['status'],
      ]);
    }

    switch ($paysonCheckout['status']) {
      case 'expired':
        // Payment has expired, delete it.
        $payment->delete();
        break;

      case 'readyToShip':
        // Update address from Payson.
        if (!empty($this->configuration['collect_billing_information'])) {
          $billingProfile = $order->getBillingProfile();
          $billingProfile->address->given_name = $paysonCheckout['customer']['firstName'];
          $billingProfile->address->family_name = $paysonCheckout['customer']['lastName'];
          $billingProfile->address->country_code = $paysonCheckout['customer']['countryCode'];
          $billingProfile->address->locality = $paysonCheckout['customer']['city'];
          $billingProfile->address->postal_code = $paysonCheckout['customer']['postalCode'];
          $billingProfile->address->address_line1 = $paysonCheckout['customer']['street'];
          $billingProfile->save();
          $order->setBillingProfile($billingProfile);
          $order->save();
        }
        $payment->setAmount(new Price($paysonCheckout['order']['totalPriceIncludingTax'], $paysonCheckout['order']['currency']));
        $payment->setRefundedAmount(new Price($paysonCheckout['order']['totalCreditedAmount'], $paysonCheckout['order']['currency']));
        $payment->setState('authorization');
        $payment->setRemoteState($paysonCheckout['status']);
        $payment->save();

        // If order should be marked as 'completed' we should ping
        // Payson that.
        if ($this->configuration['order_state'] == 'completed') {
          if ($this->configuration['log']) {
            $this->logger->debug('Sending "shipped" status to Payson for: %remoteId', [
              '%remoteId' => $remoteId,
            ]);
          }
          $result = $paysonHandler->updateStatus($payment, 'shipped');
          if (empty($result)) {
            $this->logger->error('Could not update status on payment: %payment_id ', [
              '%payment_id' => $payment->id(),
            ]);
          }
        }
        break;

      case 'shipped':
      case 'paidToAccount':
        $payment->setAmount(new Price($paysonCheckout['order']['totalPriceIncludingTax'], $paysonCheckout['order']['currency']));
        $payment->setRefundedAmount(new Price($paysonCheckout['order']['totalCreditedAmount'], $paysonCheckout['order']['currency']));
        $payment->setState('completed');
        $payment->setRemoteState($paysonCheckout['status']);
        $payment->save();
        break;

      case 'canceled':
        $payment->setRemoteState($paysonCheckout['status']);
        $payment->save();
        break;
    }
    return NULL;
  }

}
